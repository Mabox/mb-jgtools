#!/bin/bash
# Daniel Napora 2021-24 <napcok@gmail.com>
#: mb-reset - reset/update user config files to (current) distro defaults. 
#:    It only affects files shipped with Mabox.
#:    Be aware that you may loose your own customizations (if any).
#:
#: Usage: mb-reset option
#:
#: Options:
#:   Compare mode:
#:     -c           - compare files using meld GUI (if installed)
#:                    It lets you see changes and decide which one to pick or not.
#:                    Works with rc.xml,bashrc and autostart files
#:                    Usage: mb-reset -c obrcxml | autostart | bashrc
#:   Overwrite mode:
#:     obrcxml      - rc.xml default OpenBox configuration file
#:     obautostart  - default autostart file
#:     bashrc       - bash config file (~/.bashrc)
#:     picom        - install/override new picom config with animations
#:     terminator   - terminator config
#:     conky        - overwrite conky config files in ~/.config/conky/
#:     tint2        - overwrite tint2 panel config files in ~/.config/tint2/
#:     jgthemes     - overwrite menu/sidepanels color schemes in ~/.config/mabox/jgobthemes/
#:     mainmenu     - overwrite main menu files (favorites.csv)
#:     leftpanel    - left panel custom commands
#:     rightpanel   - right panel custom commands
#:     allmenus     - overvrite all menus, panels and exit dialog
#:     logout       - logout dialog
#:     labwc        - labwc session configs
#:     labwc-theme  - labwc theme for Mabox
#:     mabox-labwc  - mabox-labwc session configs
#:     waybar       - waybar configs


case $LANG in
    pl*) LNGDIR="pl";;
    es*) LNGDIR="es";;
    *) LNGDIR="en";;
esac

__usage() {
    grep "^#:" $0 | while read DOC; do printf '%s\n' "${DOC###:}"; done
    exit
}

__compare() {
    command -v meld >/dev/null 2>&1 || { echo "mb-reset compare mode requires meld. Install it and try again. (yay -S meld)" >&2; exit 1; }
    case "$1" in
        obrcxml)
        meld /usr/share/mabox/common/rc.xml ~/.config/openbox/rc.xml &
        ;;
        autostart)
        meld /usr/share/mabox/common/autostart ~/.config/openbox/autostart &
        ;;
        bashrc)
        meld /usr/share/mabox/common/.bashrc ~/.bashrc &
        ;;
        conky)
        meld /usr/share/mabox/lang/$LNGDIR/.config/conky/ ~/.config/conky/ &
        ;;
        *)
        echo "Compare "
        echo "Usage: mb-reset -c obrcxml|autostart|bashrc"
        ;;
    esac
}


__allmenus() {
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/settings* $HOME/.config/mabox/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/jgmenu/*.csv $HOME/.config/jgmenu/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/favorites.csv $HOME/.config/mabox/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/places* $HOME/.config/mabox/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/right* $HOME/.config/mabox/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/exit.csv $HOME/.config/mabox/
mb-setvar places_menusettingspipe=true
mb-setvar places_sysinfopipe=true
mb-setvar places_keyspipe=true
mb-setvar places_sshpipe=true
mb-setvar places_conkypipe=true
mb-setvar places_tint2pipe=true
mb-setvar places_jgdesktops=true
mb-setvar places_jgdeskmngr=true
mb-setvar places_softwarepipe=true

}

__mainmenu() {
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/jgmenu/*.csv $HOME/.config/jgmenu/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/favorites.csv $HOME/.config/mabox/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/setting*.csv $HOME/.config/mabox/
}

__leftpanel() {
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/places* $HOME/.config/mabox/
mb-setvar places_menusettingspipe=true
mb-setvar places_sysinfopipe=true
mb-setvar places_keyspipe=true
mb-setvar places_sshpipe=true
mb-setvar places_conkypipe=true
mb-setvar places_tint2pipe=true
mb-setvar places_jgdesktops=true
mb-setvar places_jgdeskgrid=true
mb-setvar places_softwarepipe=true
}

__rightpanel() {
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/right* $HOME/.config/mabox/
}

__logout() {
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/mabox/exit.csv $HOME/.config/mabox/
}


__tint2() {
mkdir -p $HOME/.config/fontconfig/conf.d
rsync -a /usr/share/mabox/common/66-symbols.conf $HOME/.config/fontconfig/conf.d/
fc-cache
rsync -a /usr/share/mabox/common/volumettf $HOME/.config/tint2/scripts/
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/tint2/*.tint2rc $HOME/.config/tint2/
TYPE=$(cat /sys/class/dmi/id/chassis_type)
    case "$TYPE" in
        8|9|10|14):;;
        *)
        # fix tint2 battery indicator bug spawning notifications even if there is no battery
        sd "battery_low_cmd =.*$" "battery_low_cmd =" "$HOME"/.config/tint2/*.tint2rc
        ;;
    esac
killall -SIGUSR1 tint2
}

__jgthemes() {
rm $HOME/.config/mabox/jgobthemes/*.bak
rsync -a /usr/share/mb-jgtools/jgobthemes/* $HOME/.config/mabox/jgobthemes/
}

__conky() {
cp $HOME/.config/conky/conky-sessionfile{,.bak."$(date +%Y%m%d-%H%M%S)"}
cp $HOME/.config/conky/saved-sessions{,.bak."$(date +%Y%m%d-%H%M%S)"}
rsync -a /usr/share/mabox/lang/$LNGDIR/.config/conky/* $HOME/.config/conky/
killall -SIGUSR1 conky
}

__maboxthemes() {
echo "Not implemented yet"
}

__obrcxml() {
cp $HOME/.config/openbox/rc.xml{,.bak."$(date +%Y%m%d-%H%M%S)"}
rsync -a /usr/share/mabox/common/rc.xml $HOME/.config/openbox/
openbox --reconfigure
}

__obautostart() {
cp $HOME/.config/openbox/autostart{,.bak."$(date +%Y%m%d-%H%M%S)"}
rsync -a /usr/share/mabox/common/autostart $HOME/.config/openbox/
}

__bashrc() {
cp $HOME/.bashrc{,.bak."$(date +%Y%m%d-%H%M%S)"}
rsync -a /usr/share/mabox/common/.bashrc $HOME/
}

__terminator() {
rsync -a /usr/share/mabox/common/terminator/* $HOME/.config/terminator/
}

__picom() {
mkdir -p $HOME/.config/picom/include
rsync -a /usr/share/mabox/common/picom/include/* $HOME/.config/picom/include/
rsync -a /usr/share/mabox/common/picom/configs/mabox-jaskier.conf $HOME/.config/picom/configs/
}
__labwc() {
mkdir -p $HOME/.config/labwc
rsync -a /etc/skel/.config/labwc/* $HOME/.config/labwc/
}
__mabox-labwc() {
mkdir -p $HOME/.config/mabox-labwc
rsync -a /etc/skel/.config/mabox-labwc/* $HOME/.config/mabox-labwc/
}
__waybar() {
mkdir -p $HOME/.config/waybar
rsync -a /etc/skel/.config/waybar/* $HOME/.config/waybar/
}
__labwc-theme() {
mkdir -p $HOME/.local/share/themes/Mabox-Labwc/openbox-3
rsync -a /etc/skel/.local/share/themes/Mabox-Labwc/openbox-3/* $HOME/.local/share/themes/Mabox-Labwc/openbox-3/
}

case "$1" in
    -c|check) __compare "$2";;
    allmenus) __allmenus ;;
    mainmenu) __mainmenu ;;
    leftpanel) __leftpanel ;;
    rightpanel) __rightpanel ;;
    logout) __logout ;;
    tint2) __tint2 ;;
    jgthemes) __jgthemes ;;
    conky) __conky ;;
    maboxthemes) __maboxthemes ;;
    obrcxml) __obrcxml ;;
    obautostart) __obautostart ;;
    bashrc) __bashrc ;;
    terminator) __terminator ;;
    picom) __picom ;;
    labwc) __labwc ;;
    mabox-labwc) __mabox-labwc ;;
    waybar) __waybar ;;
    labwc-theme) __labwc-theme ;;
    -h|--help) __usage ;;
    *) __usage ;;
esac

exit 0
